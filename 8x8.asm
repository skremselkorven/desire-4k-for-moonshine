// 4K for moonshine / Desire
// coded by Golara in 2019

.label scroll=$e0
.label chardata=$e2
.label charbuff = $e4
.label charrom_ptr=$d000

scroll_8x8:
// bank in CHAR ROM
        lda #$31
        sta $01
        ldx #$00
!:
        lda scrollscreenptr+$01+1,x
        sta scrollscreenptr+$00+1,x
        lda scrollscreenptr+$29+1,x
        sta scrollscreenptr+$28+1,x
        lda scrollscreenptr+$51+1,x
        sta scrollscreenptr+$50+1,x
        lda scrollscreenptr+$79+1,x
        sta scrollscreenptr+$78+1,x
        lda scrollscreenptr+$a1+1,x
        sta scrollscreenptr+$a0+1,x
        lda scrollscreenptr+$c9+1,x
        sta scrollscreenptr+$c8+1,x
        lda scrollscreenptr+$f1+1,x
        sta scrollscreenptr+$f0+1,x
        inx
        cpx #$27
        bne !-

        dec print_pos
        ldx #$07
        ldy print_pos:#$00
        bpl print_char
        iny
        sty chardata+1
        stx print_pos

        lda (scroll),y
        bne !+
reset_scroll:
        lda #<scrolltext
        sta scroll+0
        lda #>scrolltext
        sta scroll+1
        lda (scroll),y

!:
// make space narrower
        cmp #$20              
        bne !+                
        dec print_pos
        dec print_pos
        dec print_pos

!:      asl                   
        rol chardata+1        
        asl                   
        rol chardata+1        
        asl                   
        rol chardata+1        
        sta chardata          
        lda chardata+1        
        ora #>charrom_ptr        
        sta chardata+1        
        inc scroll            
        bne !+                
        inc scroll+1          
!:
        lda (chardata),y
        sta charbuff,x  
        iny             
        dex             
        bpl !-          
        ldx #$06        

print_char:
        ldy #$27
        lda #<scrollscreenptr 
        sta chardata 
        lda #>scrollscreenptr 
        sta chardata+1

print_char_loop:
        lda #$18           
        asl charbuff,x     
        rol                
        sta (chardata),y   
        lda chardata       
        adc #$28           
        sta chardata       
        lda chardata+1     
        adc #$00           
        sta chardata+1     
        dex                
        bpl print_char_loop

// disable CHAR ROM
        lda #$35
        sta $01
// modify the charset a bit for some variety.
// better idea would be to have several frames for each
// block... a circle getting smaller/bigger etc... oh well.
        lda (scroll),y
        cmp scroll+1
        rol charsetptr + $31*8 + 0
        rol charsetptr + $31*8 + 1
        rol charsetptr + $31*8 + 2
        rol charsetptr + $31*8 + 3
        rol charsetptr + $31*8 + 4
        rol charsetptr + $31*8 + 5
        rol charsetptr + $31*8 + 6
        rol charsetptr + $31*8 + 7
    
        rts

.align $100
scrolltext:
.encoding "screencode_mixed"
.text "new product you so much desire..."
.text "   hello moonshine dragons ! what's up ??!"
.text "   first some creditz..."
.text "   lda/sta by golara..."
.text "   logo by visionvortex..."
.text "   muzak by no-xs..."
.text "   so what else can i say ?"
.text "   i don't know how much i can say. this is 4k..."
.text "   only alz64 makes it possible !"
.text "   ok i think it's enough. look at the logo above"
.text " doing some cool moves :)          @"