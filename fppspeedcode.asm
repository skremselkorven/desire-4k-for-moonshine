// 4K for moonshine / Desire
// coded by Golara in 2019

// Some code that is copied many times (unrolled loops)
// Better explanation of each routine is in main.asm

.label fpp_speedcode = $a000
.label fppline = fpp_speedcode + 7
.label fppstep = $25
.label bgline = fpp_speedcode + $24
.label bgstep = $25*2

insert_rts:
        lda #RTS
        ldy #$00
        sta ($04),y
        rts

code_copy:
        ldy #$00
!:
        lda ($02), y
        sta ($04), y
        iny
        cpy codesize:#00
        bne !-

        lda $04
        clc
        adc codesize
        sta $04
        bcc !+
        inc $05
        clc
!:
        rts

fppline1:
        lda #<fppcode1s
        sta $02
        lda #>fppcode1s
        sta $03
        lda #(fppcode1e - fppcode1s)
        sta codesize
        jsr code_copy

        lda d011mod
        adc #2
        and #7
        ora #$18
        sta d011mod
        rts
fppcode1s:
        sta $d011
        stx $d017
        lda #$00
        sta fppscreen + $3f8 + 2
        sta fppscreen + $3f8 + 3
        sta fppscreen + $3f8 + 4
        sta fppscreen + $3f8 + 5
        sta fppscreen + $3f8 + 6
        sty $d017
        sty $d016
        sta fppscreen + $3f8 + 7
        stx $d016
        lda #00
fppcode1e:

fppline2:
        lda #<fppcode2s
        sta $02
        lda #>fppcode2s
        sta $03
        lda #(fppcode2e - fppcode2s)
        sta codesize
        jsr code_copy
        rts
fppcode2s:
        sta $d021
        stx $d017
        lda #$00
        sta fppscreen + $3f8 + 2
        sta fppscreen + $3f8 + 3
        sta fppscreen + $3f8 + 4
        sta fppscreen + $3f8 + 5
        sta fppscreen + $3f8 + 6
        sty $d017
        sty $d016
        sta fppscreen + $3f8 + 7
        stx $d016
        lda d011mod:#$1a
fppcode2e:

.label clearfpp = $b281
make_clearfpp:
        lda #LDA_IMM
        sta $b281
        lda #$00
        sta $b282
        
        ldy #00
!:
        lda #STA_ZP
        sta $b283, y
        iny
        lda m:#fppshaddowzp
        sta $b283, y
        inc m
        iny
        bne !-
        lda #RTS
        sta $b383
        rts


make_copy_fppshadowzp:
        lda #<lut3
        sta $02
        lda #>lut3
        sta $03
        lda #(lut3e - lut3)
        sta codesize
        jsr code_copy
        inc l3e1
        clc
        lda l3e2
        adc #fppstep
        sta l3e2
        bcc !+
        inc l3e2 + 1
!:
        rts
lut3:
        lda l3e1:fppshaddowzp + 0
        sta l3e2:fppline + 0*fppstep
lut3e:


make_scrollraster_speedcode:
        lda #<lut4
        sta $02
        lda #>lut4
        sta $03
        lda #(lut4e - lut4)
        sta codesize
        jsr code_copy
        inc l4e1
        clc
        lda l4e2
        adc #bgstep
        sta l4e2
        bcc !+
        inc l4e2 + 1
!:
        rts
lut4:
        lda l4e1:fppshaddowzp + 0
        sta l4e2:bgline + 0*bgstep
lut4e:
